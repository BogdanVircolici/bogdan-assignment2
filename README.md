const FIRST_NAME = "Bogdan";
const LAST_NAME = "Vîrcolici";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache={};
    cache.pageAccessCounter=function(sectiuneSite='home'){
        sectiuneSite=new String(sectiuneSite).toLowerCase();
        if(cache.hasOwnProperty(sectiuneSite)){
            cache[sectiuneSite]++;
        }
        else{
            Object.defineProperty(cache,sectiuneSite,{
                value:1,
                writable:true
            });
        }
    }
    cache.getCache=function(){
        return this;
    }
    return cache;   
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}